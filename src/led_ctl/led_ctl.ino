#include <FastLED.h>

FASTLED_USING_NAMESPACE

// fork of Mark Kriegsman, December 2014 demo 100
// by gllmar 2020
// Pot_0 => Brightness
// Pot_1 => Pattern
// Pot_2 => Parameter

bool DEBUG = false;

#if defined(FASTLED_VERSION) && (FASTLED_VERSION < 3001000)
#warning "Requires FastLED 3.1 or later; check github for latest code."
#endif

#define DATA_PIN    3
//#define CLK_PIN   4
#define LED_TYPE    WS2811
#define COLOR_ORDER GRB
#define NUM_LEDS    500
CRGB leds[NUM_LEDS];

uint8_t BRIGHTNESS=96;
uint8_t FRAMES_PER_SECOND=120;
uint8_t BeatsPerMinute = 62;
const int POT_COUNT=3;
uint8_t pin_mapping[POT_COUNT] = {14, 15,16}; // A0, A1, A2 on arduino nano
uint8_t pot_values[POT_COUNT];

uint8_t gCurrentPatternNumber = 0; // Index number of which pattern is current
uint8_t gHue = 0; // rotating "base color" used by many of the patterns


#define ARRAY_SIZE(A) (sizeof(A) / sizeof((A)[0]))

void rainbow()
{
  // FastLED's built-in rainbow generator
  fill_rainbow( leds, NUM_LEDS, gHue, 7);
}

void rainbowWithGlitter()
{
  // built-in FastLED rainbow, plus some random sparkly glitter
  rainbow();
  addGlitter(80);
}

void addGlitter( fract8 chanceOfGlitter)
{
  if ( random8() < chanceOfGlitter) {
    leds[ random16(NUM_LEDS) ] += CRGB::White;
  }
}

void confetti()
{
  // random colored speckles that blink in and fade smoothly
  fadeToBlackBy( leds, NUM_LEDS, 10);
  int pos = random16(NUM_LEDS);
  leds[pos] += CHSV( gHue + random8(64), 200, 255);
}

void sinelon()
{
  // a colored dot sweeping back and forth, with fading trails
  fadeToBlackBy( leds, NUM_LEDS, 20);
  int pos = beatsin16( 13, 0, NUM_LEDS - 1 );
  leds[pos] += CHSV( gHue, 255, 192);
}

void bpm()
{
  // colored stripes pulsing at a defined Beats-Per-Minute (BPM)
  CRGBPalette16 palette = PartyColors_p;
  uint8_t beat = beatsin8( BeatsPerMinute, 64, 255);
  for ( int i = 0; i < NUM_LEDS; i++) { //9948
    leds[i] = ColorFromPalette(palette, gHue + (i * 2), beat - gHue + (i * 10));
  }
}

void juggle() {
  // eight colored dots, weaving in and out of sync with each other
  fadeToBlackBy( leds, NUM_LEDS, 20);
  byte dothue = 0;
  for ( int i = 0; i < 8; i++) {
    leds[beatsin16( i + 7, 0, NUM_LEDS - 1 )] |= CHSV(dothue, 200, 255);
    dothue += 32;
  }
}
// List of patterns to cycle through.  Each is defined as a separate function below.
typedef void (*SimplePatternList[])();
SimplePatternList gPatterns = { rainbow, rainbowWithGlitter, confetti, sinelon, juggle, bpm };




bool pot_values_changed()
{
  bool changed_flag = false;
  for (int i = 0; i < POT_COUNT; i++)
  {
    int pin_value = analogRead(pin_mapping[i])>>3; //bit shift 1024 to 127 to stabilize values
    if (pin_value!=pot_values[i])
    {
      pot_values[i] = pin_value;
      changed_flag = true;
    }
  }
  if (DEBUG && changed_flag)
  {
    for (int i = 0; i < POT_COUNT; i++)
    {
      Serial.print(pot_values[i]);
      Serial.print(" ");
    }
    Serial.println();
  }
  return changed_flag;
}

void update_parameters()
{
   FastLED.setBrightness(pot_values[0]*2); // 0-255 bright
   FRAMES_PER_SECOND=(pot_values[1]>>2)+20; // 21-84 fp
   BeatsPerMinute = pot_values[1]; 
   gCurrentPatternNumber=map(pot_values[2],0,127,0,ARRAY_SIZE( gPatterns)-1);
}

void setup() {
  delay(1000); // 3 second delay for recovery

  // tell FastLED about the LED strip configuration
  FastLED.addLeds<LED_TYPE, DATA_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
  // set master brightness control
  FastLED.setBrightness(BRIGHTNESS);
  if (DEBUG){Serial.begin(9600);}
}



void loop()
{
  if(pot_values_changed())
  {
    update_parameters();
  }
  // Call the current pattern function once, updating the 'leds' array
  gPatterns[gCurrentPatternNumber]();

  // send the 'leds' array out to the actual LED strip
  FastLED.show();
  // insert a delay to keep the framerate modest
  FastLED.delay(1000 / FRAMES_PER_SECOND);

  // do some periodic updates
  EVERY_N_MILLISECONDS( 20 ) {
    gHue++;  // slowly cycle the "base color" through the rainbow
  }
}

void nextPattern()
{
  // add one to the current pattern number, and wrap around at the end
  gCurrentPatternNumber = (gCurrentPatternNumber + 1) % ARRAY_SIZE( gPatterns);
}
